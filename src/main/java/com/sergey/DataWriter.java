package com.sergey;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Set;

public class DataWriter {

    public static void write(Set<? extends Record> recordSet, String fileName) {

        try(FileWriter fileWriter = new FileWriter(fileName);
            PrintWriter printWriter = new PrintWriter(fileWriter)) {
            recordSet.forEach(p -> printWriter.println(p));
        }
        catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
