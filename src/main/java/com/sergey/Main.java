package com.sergey;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.sergey.RecordStatDate.COMPARE_BY_DATE;

public class Main {

    public static Map<LocalDate, BigDecimal> statDatesMap = new ConcurrentHashMap();
    public static Map<String, BigDecimal> statOfficeMap = new ConcurrentHashMap();

    public static void main(String[] args) {
        if(Objects.isNull(args)) {
            System.out.println("Для работы программы укажите входные параметры!");
            return;
        }
        if(args.length < 3) {
            System.out.println("Введено недостаточное количество параметров для работы приложения!");
            return;
        }
        String statDatesFile = args[0];
        String statOfficeFile = args[1];
        List<String> operationsFiles = Arrays.asList(args).subList(2, args.length);

        ExecutorService executorService = Executors.newFixedThreadPool(operationsFiles.size());
        operationsFiles.forEach(p -> {
            Runnable r = new Task(p);
            executorService.submit(r);
        });
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
        Set<RecordStatDate> statDatesSet = new TreeSet<>(COMPARE_BY_DATE);
        Set<RecordStatOffice> statOfficesSet = new TreeSet<>(RecordStatOffice.COMAPARE_BY_DATE);

        statDatesMap.forEach((k,v) -> statDatesSet.add(new RecordStatDate(k, v)));
        statOfficeMap.forEach((k,v) -> statOfficesSet.add(new RecordStatOffice(k,v)));


        DataWriter.write(statDatesSet, statDatesFile);
        DataWriter.write(statOfficesSet, statOfficeFile);
    }
}
