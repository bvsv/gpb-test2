package com.sergey;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;

public final class RecordStatDate extends Record {

    private LocalDate recordDate;
    private BigDecimal recordValue;

    public RecordStatDate(LocalDate recordDate, BigDecimal recordValue) {
        this.recordDate = recordDate;
        this.recordValue = recordValue;
    }

    public LocalDate getRecordDate() {
        return recordDate;
    }

    public BigDecimal getRecordValue() {
        return recordValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordStatDate recordStatDate = (RecordStatDate) o;
        return recordDate.equals(recordStatDate.recordDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recordDate);
    }

    @Override
    public String toString() {
        return recordDate + " " + recordValue ;
    }

    public static Comparator<RecordStatDate> COMPARE_BY_DATE = new Comparator<RecordStatDate>() {
        @Override
        public int compare(RecordStatDate o1, RecordStatDate o2) {
            return o1.recordDate.compareTo(o2.recordDate);
        }
    };
}
