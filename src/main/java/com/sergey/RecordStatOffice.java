package com.sergey;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Objects;

public class RecordStatOffice extends Record {

    private String office;
    private BigDecimal value;

    public RecordStatOffice(String office, BigDecimal value) {
        this.office = office;
        this.value = value;
    }

    public String getOffice() {
        return office;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordStatOffice that = (RecordStatOffice) o;
        return Objects.equals(office, that.office);
    }

    @Override
    public int hashCode() {
        return Objects.hash(office);
    }

    @Override
    public String toString() {
        return office + " " + value ;
    }

    public static Comparator<RecordStatOffice> COMAPARE_BY_DATE = new Comparator<RecordStatOffice>() {
        @Override
        public int compare(RecordStatOffice o1, RecordStatOffice o2) {
            return o1.office.compareTo(o2.office);
        }
    };
}
