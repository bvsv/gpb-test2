package com.sergey;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.stream.Stream;

public class Task implements Runnable {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private String fileName;

    public Task(String fileName) {
        this.fileName = fileName;
        System.out.println("Create task " + fileName);
    }

    @Override
    public void run() {
        try(Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8)) {
            stream.filter(Objects::nonNull).forEach(p -> {
                String[] args = p.split(";");
                try{
                    if(args.length > 3) {
                        LocalDate recordDate = LocalDate.parse(args[0], FORMATTER);
                        recordDate.toString();
                        String office = args[3];
                        BigDecimal payment = new BigDecimal(args[2]);

                        BigDecimal currentPaymentByDate = Main.statDatesMap.get(recordDate);
                        if(Objects.isNull(currentPaymentByDate)) {
                            Main.statDatesMap.put(recordDate, payment);
                        } else {
                            Main.statDatesMap.put(recordDate, currentPaymentByDate.add(payment));
                        }

                        BigDecimal currPaymentByOffice = Main.statOfficeMap.get(office);
                        if(Objects.isNull(currPaymentByOffice)) {
                            Main.statOfficeMap.put(office, payment);
                        } else {
                            Main.statOfficeMap.put(office, currPaymentByOffice.add(payment));
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
